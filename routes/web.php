<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\Auth\ChangePasswordController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\Auth\NewPasswordController;
use App\Http\Controllers\Auth\PasswordResetLinkController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\JenisMitraController;
use App\Http\Controllers\KerjaSamaController;
use App\Http\Controllers\LandingController;
use App\Http\Controllers\ProdiController;
use App\Http\Controllers\SuperAdminController;
use App\Http\Controllers\ViewController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MitraController;
use App\Http\Controllers\TemplateController;
use App\Http\Controllers\StatusKerjasamaController;
use App\Http\Controllers\ProfileController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route untuk guest
Route::middleware('guest')->group(function () {
    Route::get('/', [LandingController::class, 'showLanding']);
    Route::get('/templates/{template}/download', [LandingController::class, 'downloadTemplates'])->name('templates.download');
    Route::get('/form', [LandingController::class, 'showForm']);

    Route::get('/login', [LoginController::class, 'create'])->name('login');
    Route::post('/session/login', [LoginController::class, 'login']);

    Route::get('lupa-password', [PasswordResetLinkController::class, 'create'])->name('lupa-password');
    Route::post('lupa-password', [PasswordResetLinkController::class, 'store']);

    Route::get('reset-password/{token}', [NewPasswordController::class, 'create'])->name('password.reset');
    Route::post('reset-password', [NewPasswordController::class, 'store'])->name('password.store');
});

Route::post('/add-datakerjasama', [KerjaSamaController::class, 'storeAdmin']);

// Route untuk user yang sudah login
Route::middleware('auth')->group(function () {
    Route::get('/dashboard', DashboardController::class)->name('dashboard');

    // Route untuk super admin
    Route::middleware('role:Super Admin')->group(function () {
        Route::get('/manajemen-pengguna', [UserController::class, 'index'])->name('pengguna.manajemen-pengguna');
        Route::get('/manajemen-pengguna/tambah', [UserController::class, 'create'])->name('pengguna.manajemen-pengguna.tambah');
        Route::post('/manajemen-pengguna', [UserController::class, 'store'])->name('pengguna.manajemen-pengguna.store');
        Route::get('/manajemen-pengguna/{user}/edit', [UserController::class, 'edit'])->name('pengguna.manajemen-pengguna.edit');
        Route::put('/manajemen-pengguna/{user}', [UserController::class, 'update'])->name('pengguna.manajemen-pengguna.update');
        Route::get('/manajemen-pengguna/{user}', [UserController::class, 'show'])->name('pengguna.manajemen-pengguna.show');
        Route::post('/update-status', [UserController::class, 'updateStatus'])->name('update.status');
        Route::get('/export-users', [UserController::class, 'exportUsers']);
    });

    Route::middleware('role:Super Admin,Admin,User')->group(function () {
        // Route untuk data kerjasama
        Route::get('/export-kerjasama', [KerjaSamaController::class, 'export']);
        Route::get('/export-kerjasamaBerakhir', [KerjaSamaController::class, 'exportDataBerakhir'])->name('export-data-berakhir');
        Route::get('/export-kerjasamaAktif', [KerjaSamaController::class, 'exportDataAktif'])->name('export-data-Aktif');
        Route::get('/export-kerjasamaAkanBerakhir', [KerjaSamaController::class, 'exportDataAkanBerakhir'])->name('export-data-Akan-Berakhir');
        Route::get('/download/{file}', [KerjaSamaController::class, 'download'])->name('download-data');
        Route::get('/kerjasama', [AdminController::class, 'showKerjasama']);
        Route::get('/kerjasama-Berakhir', [AdminController::class, 'ShowKerjasamaBerakhir']);
        Route::get('/kerjasama-Aktif', [AdminController::class, 'ShowKerjasamaAktif']);
        Route::get('/kerjasama-AkanBerakhir', [AdminController::class, 'ShowKerjasamaAkanBerakhir']);
        Route::put('/kerjasama/{id}', [KerjaSamaController::class, 'update'])->name('kerjasama-update');
    });

    // Route untuk admin dan super admin
    Route::middleware('role:Super Admin,Admin')->group(function () {
        Route::apiResource('template', TemplateController::class)->except(['show', 'destroy']);
        Route::post('template/toggle-status/{template}', [TemplateController::class, 'toggleStatus'])->name('template.toggle-status');
        Route::get('template/download/{template}', [TemplateController::class, 'downloadTemplate'])->name('template.download');

        // Route untuk jenis mitra direferensi
        Route::apiResource('/jenis-mitra', JenisMitraController::class)->except('show');
        Route::get('/jenis-mitra/export', [JenisMitraController::class, 'export'])->name('jenis-mitra.export');

        // Route untuk status mitra direferensi
        Route::get('/status-mou', [StatusKerjasamaController::class, 'index'])->name('status.index');
        Route::post('/status-mou', [StatusKerjasamaController::class, 'store'])->name('status.store');
        Route::put('/status/{status_kerjasama}', [StatusKerjasamaController::class, 'update'])->name('status.update');
        Route::delete('/status/{status_kerjasama}', [StatusKerjasamaController::class, 'destroy'])->name('status.destroy');
        Route::get('/status-mou/{status-mou}', [StatusKerjasamaController::class, 'show'])->name('status.show');
        Route::get('/export-status', [StatusKerjasamaController::class, 'exportStatus'])->name('status.export');

        // Route untuk program studi di referensi
        Route::get('/program-studi', [ProdiController::class, 'index'])->name('prodi.index');
        Route::get('/program-studi/export', [ProdiController::class, 'exportProdi'])->name('prodi.export');
        Route::post('/program-studi', [ProdiController::class, 'store'])->name('prodi.store');
        Route::delete('/program-studi/{prodi}', [ProdiController::class, 'destroy'])->name('prodi.destroy');
        Route::put('/program-studi/{prodi}', [ProdiController::class, 'update'])->name('prodi.update');


        // Route untuk mitra
        Route::get('/mitra', [MitraController::class, 'index'])->name('mitra.index');
        Route::get('/mitra/{mitra}', [MitraController::class, 'show'])->name('mitra.show');
        Route::post('/mitra', [MitraController::class, 'store'])->name('mitra.store');
        Route::get('/mitra/{mitra}/edit', [MitraController::class, 'edit'])->name('mitra.edit');
        Route::put('/mitra/{mitra}', [MitraController::class, 'update'])->name('mitra.update');
        Route::delete('/mitra/{id}', [MitraController::class, 'destroy'])->name('mitra.destroy');
        Route::get('/export-mitra', [MitraController::class, 'exportMitra'])->name('mitra.export');
    });

    Route::get('/download/{file}', [KerjaSamaController::class, 'download'])->name('download-data');
    Route::post('/kerjsama', [KerjaSamaController::class, "delete"])->name('data.destroy');

    // Rooute ganti kata sandi
    Route::get('/change-password', [ChangePasswordController::class, 'show'])->name('password.show');
    Route::post('/change-password', [ChangePasswordController::class, 'store'])->name('password.store');

    Route::get('/profil', [ProfileController::class, 'show'])->name('profil.show');
    Route::get('/edit-profil', [ProfileController::class, 'edit'])->name('profil.edit');
    Route::put('/profil/update/{user}', [ProfileController::class, 'update'])->name('profil.update');
    Route::put('/profil/update-foto/{user}', [ProfileController::class, 'updateFoto'])->name('profil.update.foto');
    Route::get('/logout', [LoginController::class, 'logout']);
});

Route::get('/hello', [ViewController::class, 'showHello']);
