@extends('base')
@section('title', 'SIMKERMA | Data Kerja Sama')
@section('konten')
    <div class="page-title-box">
        <div class="row align-items-center">
            <div class="col-md-8">
                <h6 class="page-title">Semua Data Kerja Sama</h6>
                {{-- Breadcrumb --}}
            </div>
            <div class="col-md-4">
                <div class="d-flex flex-column flex-md-row justify-content-end align-items-md-center gap-3">
                    @if($jenis === 'Berakhir')
                    <button type="button" class="btn btn-primary waves-effect waves-light" onclick="window.location.href='/export-kerjasamaBerakhir'"><i
                            class="mdi mdi-content-save"></i> Ekspor</button>
                    @elseif($jenis === 'AKtif')
                    <button type="button" class="btn btn-primary waves-effect waves-light" onclick="window.location.href='/export-kerjasamaAktif'"><i
                        class="mdi mdi-content-save"></i> Ekspor</button>
                    @elseif($jenis === 'Akan Berakhir')
                    <button type="button" class="btn btn-primary waves-effect waves-light" onclick="window.location.href='/export-kerjasamaAkanBerakhir'"><i
                        class="mdi mdi-content-save"></i> Ekspor</button>
                    @else
                    <button type="button" class="btn btn-primary waves-effect waves-light" onclick="window.location.href='/export-kerjasama'"><i
                        class="mdi mdi-content-save"></i> Ekspor</button>
                    @endif    
                    <button type="button" class="btn btn-primary waves-effect waves-light" data-bs-toggle="modal"
                        data-bs-target="#modalTambah"><i class="mdi mdi-plus"></i>
                        Tambah Kerja Sama Baru</button>
                </div>
            </div>
        </div>
    </div>
    {{-- Data Tabel --}}
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-rep-plugin">
                        <div class="table-responsive mb-0" data-bs-pattern="priority-columns">
                            <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th data-priority="1">Nama Instansi</th>
                                        <th data-priority="2">Nama Kegiatan</th>
                                        <th data-priority="3">Tanggal Mulai</th>
                                        <th data-priority="3">Status</th>
                                        <th data-priority="1">Edit</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($dataKerjasama as $item)
                                        <tr>
                                            <th><span class="co-name">{{ $loop->iteration }}</span></th>
                                            <td>{{ $item->nama_mitra }}</td>
                                            <td>{{ $item->deskripsi }}</td>
                                            <td>{{ $item->tanggal_mou }}</td>
                                            <td>{{ $item->status }}</td>
                                            <td>
                                                <div class="row gap">
                                                    <div class="col">  
                                                        <button type="button" class="btn btn-secondary waves-effect" data-bs-toggle="modal" data-bs-target="#modalView{{ $item->id }}"><i
                                                                class="mdi mdi-eye" 
                                                                ></i></button>
                                                        @auth
                                                            @if (Auth::user()->role === 'Admin' || Auth::user()->role === 'Super Admin')
                                                            <button type="button" class="btn btn-warning waves-effect" data-bs-toggle="modal"  data-bs-target="#modalEdit{{ $item->id }}"><i
                                                                    class="mdi mdi-pencil" style="color: black"
                                                                    ></i></button>
                                                            <button type="button" class="btn btn-danger waves-effect deleteBtn" id="deleteBtn" data-id="{{ $item->id }}"><i class="mdi mdi-trash-can" style="color: black"></i></button>
                                                            <a href="{{ route('download-data', $item->file) }}">
                                                                <button type="button" class="btn btn-info waves-effect waves-light" id="downloadBtn" data-id="{{$item->id}}"><i class="mdi mdi-file-download" style="color: black"></i></button>
                                                            </a>
                                                            @endif
                                                        @endauth
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->

    {{-- MODAL TAMBAH KERJA SAMA BARU --}}
    <div id="modalTambah" class="modal fade bs-example-modal-center" tabindex="-1" role="dialog"
        aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <form action="/add-datakerjasama" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="myModalLabel2">Tambah Kerja Sama Baru</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="col">
                            <div class="col-md-12 pb-3">
                                <label for="nama-instansi" class="form-label">Nama Instansi</label>
                                {{-- <input type="text" class="form-control" id="nama-instansi" name="nama_mitra" required> --}}
                                <select class="form-select" aria-label="Default select example" id="nama-instansi" name="mitra_id" required>
                                    <option selected>Pilih Mitra</option>
                                    @foreach ($Mitra as $item)
                                        <option value="{{ $item->id }}">{{ $item->nama_mitra }}</option>
                                    @endforeach
                                  </select>
                            </div>
                            <div class="col-md-12 pb-3">
                                <label for="nama-kegiatan" class="form-label">Nama Kegiatan</label>
                                <input type="text" class="form-control" id="nama-kegiatan" name="deskripsi" required>
                            </div>
                            <div class="col-md-12 pb-3">
                                <label for="judul-mou" class="form-label">Nomor surat mitra</label>
                                <input type="text" class="form-control" id="judul-mou" name="no_surat_mitra" required>
                            </div>
                            <div class="col-md-12 pb-3">
                                <label class="form-label">Tanggal Mulai</label>
                                <div class="input-group" id="datepicker1">
                                    <input type="date" class="form-control" placeholder="yyyy-mm-dd" name="tanggal_mou" required>

                                    <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                </div><!-- input-group -->
                            </div>
                            <div class="col-md-12 pb-3">
                                <label class="form-label">Tanggal Berakhir</label>
                                <div class="input-group" id="datepicker1">
                                    <input type="date" class="form-control" placeholder="yyyy-mm-dd" name="tanggal_akhir" required>
                                    <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                </div><!-- input-group -->
                            </div>
                            <div class="col-md-12 pb-3">
                                <label for="judul-mou" class="form-label">Dokumen MoU</label>
                                <div class="col-sm-6">
                                    <div class="file-upload-wrapper" data-text="Pilih dokumen MoU">
                                        <input name="files" type="file" class="file-upload-field" value="" accept=".pdf" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    {{-- MODAL EDIT --}}
    @foreach ($dataKerjasama as $item)
    <div id="modalEdit{{ $item ->id }}" class="modal fade bs-example-modal-center" tabindex="-1" role="dialog"
        aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <form action="{{ route('kerjasama-update', $item->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="modal-header">
                        <h5 class="modal-title" id="myModalLabel2">Edit Data Kerja Sama</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="col">
                            <div class="col-md-12 pb-3">
                                <label for="nama-kegiatan" class="form-label">Nama Kegiatan</label>
                                <input type="text" class="form-control" id="nama-kegiatan" name="deskripsi" value="{{ $item->deskripsi }}" >
                            </div>
                            <div class="col-md-12 pb-3">
                                <label for="judul-mou" class="form-label">Nomor Surat Mitra</label>
                                <input type="text" class="form-control" id="judul-mou" name="no_surat_mitra" value="{{ $item->no_surat_mitra }}" >
                            </div>
                            <div class="col-md-12 pb-3">
                                <label for="nomor-surat-instansi" class="form-label">Nomor Surat Instansi</label>
                                <input type="text" class="form-control" id="nomor-surat-instansi" name="no_surat_instansi" value="{{ $item->no_surat_instansi }}" >
                            </div>
                            <div class="col-md-12 pb-3">
                                <label for="status" class="form-label">Status</label>
                                <select name="status" class="form-select" aria-label="Default select example">
                                    @foreach($status as $statuss)
                                        <option value="{{ $statuss->nama }}" {{ $item->status === $statuss->nama ? 'selected' : '' }}>{{ $statuss->nama }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-12 pb-3">
                                <label for="jenis-dokumen" class="form-label">Jenis Dokumen</label>
                                <select name="jenis_dokumen" class="form-select" aria-label="Default select example">
                                    @php
                                        $jenis = ["MOU", "LOA"];
                                    @endphp
                                    @if(empty($item->jenis_dokumen))
                                        <option selected disabled>Silahkan pilih jenis dokumen</option>
                                    @endif
                                    @foreach($jenis as $doc)
                                        <option value="{{ $doc }}" {{ $item->jenis_dokumen === $doc ? 'selected' : '' }}>{{ $doc }}</option>
                                    @endforeach
                                </select>                                
                            </div>
                            <div class="col-md-12 pb-3">
                                <label class="form-label">Tanggal Mulai</label>
                                <div class="input-group" id="datepicker1">
                                    <input type="date" class="form-control" placeholder="yyyy-mm-dd" name="tanggal_mou" value="{{ $item->tanggal_mou }}">

                                    <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                </div><!-- input-group -->
                            </div>
                            <div class="col-md-12 pb-3">
                                <label class="form-label">Tanggal Berakhir</label>
                                <div class="input-group" id="datepicker1">
                                    <input type="date" class="form-control" placeholder="yyyy-mm-dd" name="tanggal_akhir" value="{{ $item->tanggal_akhir }}">

                                    <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                </div><!-- input-group -->
                            </div>
                            <div class="col-md-12 pb-3">
                                <label for="judul-mou" class="form-label">Dokumen MoU</label>
                                <div class="col-sm-6">
                                    <div class="file-upload-wrapper" data-text="Pilih dokumen MoU">
                                        <input name="file" type="file" class="file-upload-field" value="" accept=".pdf">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    @endforeach

    {{-- MODAL VIEW --}}
    @foreach ($dataKerjasama as $item)
    <div id="modalView{{ $item->id }}" class="modal fade bs-example-modal-center" tabindex="-1" role="dialog"
        aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <form action="" method="POST">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="myModalLabel2">Detail Data Kerja Sama</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="col">
                            <div class="col-md-12 pb-3">
                                <label for="nama-penanggung-jawab" class="form-label">Nama Penanggung Jawab</label>
                                <input type="text" class="form-control" id="nama-penanggung-jawab{{ $item->id }}" value="{{ $item->penanggung_jawab }}" name="nama-penanggung-jawab" disabled>
                            </div>
                            <div class="col-md-12 pb-3">
                                <label for="alamat-email" class="form-label">Alamat Email</label>
                                <input type="email" class="form-control" id="alamat-email{{ $item->id }}" value="{{ $item->email }}" name="alamat-email" disabled>
                            </div>
                            <div class="col-md-12 pb-3">
                                <label for="nama-instansi" class="form-label">Nama Instansi</label>
                                <input type="text" class="form-control" id="nama-instansi{{ $item->id }}" value="{{ $item->nama_mitra }}" name="instansi" disabled>
                            </div>
                            <div class="col-md-12 pb-3">
                                <label for="jenis_mitra" class="form-label">Jenis Instansi</label>
                                <input type="text" class="form-control" id="jenis_mitra{{ $item->id }}" value="{{ $item->jenis_mitra }}" name="Jenis_mitra" disabled>
                            </div>
                            <div class="col-md-12 pb-3">
                                <label for="nama-kegiatan" class="form-label">Nama Kegiatan</label>
                                <input type="text" class="form-control" id="nama-kegiatan{{ $item->id }}" value="{{ $item->deskripsi }}" name="nama-kegiatan" disabled>
                            </div>
                            <div class="col-md-12 pb-3">
                                <label for="Nomor-surat-mitra" class="form-label">Nomor Surat Mitra</label>
                                <input type="text" class="form-control" id="Nomor-surat-mitra{{ $item->id }}" value="{{ $item->no_surat_mitra }}" name="Nomor-surat-mitra" disabled>
                            </div>
                            <div class="col-md-12 pb-3">
                                <label for="nomor-surat-instansi" class="form-label">Nomor Surat instansi</label>
                                <input type="text" class="form-control" id="nomor-surat-instansi{{ $item->id }}" value="{{ $item->no_surat_instansi }}" name="nomor-surat-instansi" disabled>
                            </div>
                            <div class="col-md-12 pb-3">
                                <label for="status" class="form-label">status</label>
                                <input type="text" class="form-control" id="status{{ $item->id }}" value="{{ $item->status }}" name="status" disabled>
                            </div>
                            <div class="col-md-12 pb-3">
                                <label for="Jenis-Dokumen" class="form-label">Jenis Dokumen</label>
                                <input type="text" class="form-control" id="Jenis-Dokumen{{ $item->id }}" value="{{ $item->jenis_dokumen }}" name="Jenis-Dokumen" disabled>
                            </div>
                            <div class="col-md-12 pb-3">
                                <label class="form-label">Tanggal Mulai</label>
                                <div class="input-group" id="datepicker1">
                                    <input type="date" class="form-control" value="{{ $item->tanggal_mou }}" placeholder="yyyy-mm-dd" disabled>

                                    <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                </div><!-- input-group -->
                            </div>
                            <div class="col-md-12 pb-3">
                                <label class="form-label">Tanggal Berakhir</label>
                                <div class="input-group" id="datepicker1">
                                    <input type="date" class="form-control" value="{{ $item->tanggal_akhir }}" placeholder="yyyy-mm-dd" disabled>

                                    <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                </div><!-- input-group -->
                            </div>
                            <div class="col-md-12 pb-3">
                                <label for="judul-mou" class="form-label">Dokumen MoU</label>
                                <div class="col-sm-6">
                                    <div class="file-upload-wrapper" data-text="Pilih dokumen MoU">
                                        <a href="{{ route('download-data', $item->file) }}" class="btn bg-secondary">Unduh</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    @endforeach
@endsection

@push('script')
<script>
    $(document).ready(function() {
        $(".deleteBtn").click(function(){
            var id = $(this).data('id');
                Swal.fire({
                    title : 'Apakah Anda Yakin ?',
                    text : "Anda tidak akan dapat mengembalikan ini!",
                    icon : 'warning',
                    showCancelButton : true,
                    confirmButtonColor : '#d33',
                    cancelButtonColor : '#3085d6',
                    confirmButtonText : "Ya, Hapus Data!"
                }).then((result) => {
                    if(result.isConfirmed){
                       $.ajax({
                        type: "POST",
                        url: "{{ route('data.destroy') }}",
                        data: {
                            _token: "{{ csrf_token() }}",
                            id : id
                        },
                        success : function(response) {
                            Swal.fire({ 
                                title : "Sukses !",
                                text : "Data Berhasil Dihapus!",
                                icon : "success"
                            }).then((result) => {
                                location.reload();
                            })
                        },
                        error : function(error) {
                            Swal.fire({
                                icon : 'error',
                                title : 'Oops...',
                                text : "Terjadi Kesalahan!"
                            })
                        }
                       })
                    } else if (result.dismiss === Swal.DismissReason.cancel) {
                        Swal.fire (
                            'Batal',
                            'Tidak ada perubahan yang dilakukan.',
                            'info'
                        ).then((result) => {
                            location.reload();
                        })
                    }
                }
            )
            })
        })
</script>
@endpush