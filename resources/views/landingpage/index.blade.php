<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SIMKERMA</title>
    <!-- Icon Title -->
    <link rel="shortcut icon" href="assets/images/simkerma-logo-small.png">
    <!-- Bootstrap Css -->
    <link href="assets/css/bootstrap.min.css" id="bootstrap-style" rel="stylesheet" type="text/css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css" integrity="sha512-DTOQO9RWCH3ppGqcWaEA1BIZOC6xxalwEsw9c2QQeAIftl+Vegovlnee1c9QX4TctnWMn13TZye+giMm8e2LwA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!-- CSS -->
    <link rel="stylesheet" href="assets/css/landing.css">
    <!-- SWIPER CSS -->
    <link rel="stylesheet" href="assets/css/swiper-bundle.min.css">
    <!-- AOS -->
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
</head>

<body>
    <!-- Top Navbar -->
    <div class="container top-navbar text-center p-3">
        <div class="row justify-content-md-center">
            <div class="col col-md-2">
                <h6>
                    Layanan Gawat Darurat<br></h6>
                <h5>0811-3277-300</h5>
            </div>
            <div class="col col-md-2">
                <h6>
                    Pusat Informasi<br></h6>
                <h5>0811-3229-9222</h5>
            </div>
            <div class="col col-md-2">
                <h6>
                    Pengaduan Layanan<br></h6>
                <h5>0811-3494-390</h5>
            </div>
        </div>
    </div>

    <!-- BG Navbar -->
    <div class="square"></div>
    <div class="square2">
        <img src="assets/images/landing/artis-min 1.png" data-aos="fade-down" data-aos-duration="3000">
    </div>

    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg">
        <div class="container-fluid">
            <a href="#" class="navbar-brand">
                <img src="assets/images/cropped-logo-itsk-dr-soepraoen 1.png" alt="Logo" height="70">
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fas fa-bars"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ms-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="#profile">Profil</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#program-studi">Program Studi</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#tenaga-pengajar">Tenaga Pengajar</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#daftar-mou">Daftar MoU</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link nav-color" href="#daftar-kerja-sama">Daftar Kerjasama</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link nav-color" href="#footer">Kontak Kami</a>
                    </li>
                    <li class="nav-item px-5">
                        <a href="{{ url('/login') }}" class="button" role="button" style="color: #ffff;">Masuk<i class="fas fa-arrow-right"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <!-- Info Head Landing -->
    <div class="container info">
        <div class="row">
            <div class="col-md-6">
                <h5 data-aos="zoom-in" data-aos-duration="3000">Selamat Datang di Website</h5><br>
                <h3 data-aos="fade-up" data-aos-delay="300" data-aos-duration="1000">ITSK RS dr. <br> Soepraoen <br> Malang</h3>
                <p data-aos="fade-up" data-aos-delay="300" data-aos-duration="2000">Kampus kesehatan unggulan di Jawa Timur,<br>
                    Peluang kerja ke luar negeri, <br>
                    Pertukaran pelajar ke luar negeri, <br>
                    Dosen dengan penelitian bereputasi internasional, <br>
                    dan Rumah sakit milik sendiri.</p>
                <a href="#profile" class="button" role="button">Jelajahi Sekarang</a>
                <a href="/form" class="button" role="button">Ajukan Kerjasama</a>
            </div>
        </div>
    </div>

    <!-- Profile Section -->
    <section id="profile" style="background-color: #D1EEFF; margin-top: 50px">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 profile">
                    <div class="profile-line"></div>
                    <h6 data-aos="fade-right" data-aos-delay="500">Kenapa harus memilih <br> ITSK Soepraoen?</h6>

                    <p data-aos="fade-up" data-aos-delay="600">Keunggulan Institut Teknologi Sains dan Kesehatan RS dr. <br>
                        Soepraoen Kesdam V/BRW Malang siap memberikan pelayanan<br>
                        pendidikan terbaik</p>
                </div>
                <div class="col-md-6 profile-img" data-aos="zoom-in-down" data-aos-duration="3000">
                    <img src="assets/images/cropped-logo-itsk-dr-soepraoen 1.png" alt="itsk logo">
                </div>
            </div>

            <div class="stats">
                <div class="stat" style="border-radius: 5px 0 0 5px;">
                    <p>13</p>
                    <p>Program Studi</p>
                </div>
                <div class="stat">
                    <p>1300</p>
                    <p>Total Mahasiswa</p>
                </div>
                <div class="stat">
                    <p>100</p>
                    <p>Dosen</p>
                </div>
                <div class="stat">
                    <p>5000</p>
                    <p>Animo Pendaftar</p>
                </div>
                <div class="stat" style="border-radius: 0 5px 5px 0; border-right: none">
                    <p>800</p>
                    <p>Pendaftar Lolos</p>
                </div>
            </div>

            <div class="row pt-5 mx-0 my-0" style="--bs-gutter-x: 0;">
                <div class="col-md-4 text-center" data-aos="fade-up" data-aos-duration="2000">
                    <img src="assets/images/profile1.png" width="100px" alt="logo 1">
                    <p class="fw-bold">Dosen bereputasi <br> Internasional</p>
                </div>
                <div class="col-md-4 text-center" data-aos="fade-up" data-aos-duration="2000">
                    <img src="assets/images/profile2.png" width="100px" alt="logo 2">
                    <p class="fw-bold mb-0">Kampus Unggulan Di <br> Jajaran KESAD</p>
                    <p>terakreditasi B</p>
                </div>
                <div class="col-md-4 text-center" data-aos="fade-up" data-aos-duration="2000">
                    <img src="assets/images/profile3.png" width="100px" alt="logo 3">
                    <p class="fw-bold mb-0">Lulusan bekerja di Luar <br> Negeri</p>
                    <p>Jepang - Taiwan</p>
                </div>
            </div>
        </div>
    </section>

    <!-- Prodi Section -->
    <section id="program-studi">
        <div class="container-fluid mt-2">
            <div class="title text-center pt-4 pb-4" data-aos="fade-up" data-aos-duration="1000">
                <h1>Program Studi</h1>
            </div>
            <div class="card-prodi p-5">
                <div class="row m-5 gap-5">
                    <div class="col-md-3 offset-md-1">
                        <div class="card h-100" data-aos="fade-up" data-aos-delay="300" data-aos-duration="1000">
                            <div class="card-body">
                                <img class="img-card" src="assets/images/icon cool-icon-11.png" width="50px" height="50px">
                                <h5 class="card-title pt-2">Jenjang Sarjana / Sarjana Terapan</h5>
                                <p class="card-text">
                                <ul class="list-prodi">
                                    <li>S1 Kebidanan</li>
                                    <li>S1 Farmasi Klinis dan Komunitas</li>
                                    <li>S1 Fisioterapi</li>
                                    <li>S1 Informatika</li>
                                    <li>S1 Keperawatan</li>
                                    <li>S1 Sarjana Terapan Keperawatan Anestesiologi</li>
                                </ul>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card h-100" data-aos="fade-up" data-aos-delay="500" data-aos-duration="1000">
                            <div class="card-body">
                                <img class="img-card" src="assets/images/icon cool-icon-18.png" width="50px" height="50px">
                                <h5 class="card-title pt-2">Jenjang Diploma</h5>
                                <p class="card-text">
                                <ul class="list-prodi">
                                    <li>D3 Keperawatan</li>
                                    <li>D3 Akunpuntur</li>
                                    <li>D3 Farmasi</li>
                                    <li>D3 Rekam Medis dan Informasi Kesehatan</li>
                                </ul>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card h-100" data-aos="fade-up" data-aos-delay="700" data-aos-duration="1000">
                            <div class="card-body">
                                <img class="img-card" src="assets/images/icon cool-icon-156.png" width="50px" height="50px">
                                <h5 class="card-title pt-2">Jenjang Profesi</h5>
                                <p class="card-text">
                                <ul class="list-prodi">
                                    <li>Profesi Bidan</li>
                                    <li>Profesi Ners</li>
                                </ul>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Dosen Section -->
    <section id="tenaga-pengajar">
        <div class="container pt-5 pb-5">
            <div class="title text-center" data-aos="fade-up" data-aos-duration="1000">
                <h1>Tenaga Pengajar</h1>
                <p>Ingin Mendaftar? Cek tenaga pengajar kami</p>
            </div>
            <div class="container-card">
                <div class="card-container swiper">
                    <div class="card-content">
                        <div class="swiper-wrapper">
                            <article class="card-article swiper-slide">
                                <div class="card-image">
                                    <img src="assets/images/landing/dosen-1.png" alt="image" class="card-dosen">
                                </div>

                                <div class="card-data">
                                    <h3 class="card-title" style="color: #23A6F0">Sarjana Informatika</h3>
                                    <h3 class="card-name">M. Syauqi Haris, S.Kom., M.Kom</h3>
                                    <p class="card-description">
                                        Ketua Program Studi Sarjana Informatika
                                    </p>
                                </div>
                            </article>

                            <article class="card-article swiper-slide">
                                <div class="card-image">
                                    <img src="assets/images/landing/dosen-2.png" alt="image" class="card-dosen">
                                </div>

                                <div class="card-data">
                                    <h3 class="card-title" style="color: #23A6F0">Sarjana Farmasi Klinis dan Komunitas
                                    </h3>
                                    <h3 class="card-name">Ratih Tyas Widara, S.Si., M.Si</h3>
                                    <p class="card-description">
                                        Dosen Sarjana Farmasi Klinis dan Komunitas
                                    </p>
                                </div>
                            </article>

                            <article class="card-article swiper-slide">
                                <div class="card-image">
                                    <img src="assets/images/landing/dosen-3.png" alt="image" class="card-dosen">
                                </div>

                                <div class="card-data">
                                    <h3 class="card-title" style="color: #23A6F0">Sarjana Terapan Keperawatan dan
                                        Anestiologi</h3>
                                    <h3 class="card-name">Suryanto, S.Kep.,Ners, S.Tr.Kes.,M.Kes</h3>
                                    <p class="card-description">
                                        Dosen Sarjana Terapan Keperawatan dan Anestiologi
                                    </p>
                                </div>
                            </article>

                            <article class="card-article swiper-slide">
                                <div class="card-image">
                                    <img src="assets/images/landing/dosen-3.png" alt="image" class="card-dosen">
                                </div>

                                <div class="card-data">
                                    <h3 class="card-title" style="color: #23A6F0">Sarjana Terapan Keperawatan dan
                                        Anestiologi</h3>
                                    <h3 class="card-name">Suryanto, S.Kep.,Ners, S.Tr.Kes.,M.Kes</h3>
                                    <p class="card-description">
                                        Dosen Sarjana Terapan Keperawatan dan Anestiologi
                                    </p>
                                </div>
                            </article>

                            <article class="card-article swiper-slide">
                                <div class="card-image">
                                    <img src="assets/images/landing/dosen-2.png" alt="image" class="card-dosen">
                                </div>

                                <div class="card-data">
                                    <h3 class="card-title" style="color: #23A6F0">Sarjana Farmasi Klinis dan Komunitas
                                    </h3>
                                    <h3 class="card-name">Ratih Tyas Widara, S.Si., M.Si</h3>
                                    <p class="card-description">
                                        Dosen Sarjana Farmasi Klinis dan Komunitas
                                    </p>
                                </div>
                            </article>
                        </div>
                    </div>

                    <!-- Navigation buttons -->
                    <div class="swiper-button-next">
                        <img src="assets/images/landing/right.png">
                    </div>

                    <div class="swiper-button-prev">
                        <img src="assets/images/landing/left.png">
                    </div>
                </div>
            </div>
    </section>

    <!-- MoU Section -->
    <section id="daftar-mou">
        <div class="container-fluid pt-2">
            <div class="title text-center" data-aos="fade-up" data-aos-duration="1000">
                <h1>Daftar MoU</h1>
                <p class="info">Ingin mengajukan kerjasama? Cek draft MoU yang Anda butuhkan</p>
            </div>
            <div class="group-mou">
                <div class="row">
                    @foreach ($templates as $data)
                    <div class="col-sm-4 px-3">
                        <div class="card" data-aos="zoom-in-up" data-aos-duration="1000">
                            <div class="card-body">
                                <img class="img-card" src="{{ asset('assets/images/landing/daftar-mou.png') }}" width="50px" height="50px">
                                <h5 class="card-title pt-2">{{ $data->nama }}</h5>
                                <div class="card-border mb-3"></div>

                                <div class="d-flex justify-content-center mt-auto">
                                    <a href="{{ route('templates.download', $data->id) }}" class="btn bg-custom text-light w-100 fw-bold">Unduh MoU</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>

    <!-- Kerjasama Section -->
    <section id="daftar-kerja-sama">
        <div class="container pt-5 pb-5">
            <div class="title text-center" data-aos="fade-up" data-aos-duration="1000">
                <h1>Daftar Kerja Sama</h1>
                <p>Ingin bekerja sama dengan kami seperti mereka?</p>
            </div>
            <div class="container swiper pb-5">
                <div class="slide-container pb-3">
                    <div class="card-wrapper swiper-wrapper">
                        <div class="card-kerjasama swiper-slide text-center">
                            <div class="image-box pt-5">
                                <img src="assets/images/landing/kerjasama-1.png" alt="kerjasama-1" />
                            </div>
                            <div class="profile-details">
                                <h5>Persatuan Perawat Nasional Indonesia <br> (PPNI)</h5>
                                <div class="card-border mb-3" style="margin: 0 auto"></div>
                                <p>Pengelolaan dan Penerbitan Jurnal Ilmiah</p>
                            </div>
                        </div>
                        <div class="card-kerjasama swiper-slide text-center">
                            <div class="image-box pt-5">
                                <img src="assets/images/landing/kerjasama-2.png" alt="kerjasama-2" />
                            </div>
                            <div class="profile-details">
                                <h5>Rumah Sakit Wava Husada Kepanjen</h5>
                                <div class="card-border mb-3" style="margin: 0 auto"></div>
                                <p>Pelaksanaan Pendidikan, Pelatihan dan Pengabdian Masyarakat</p>
                            </div>
                        </div>
                        <div class="card-kerjasama swiper-slide text-center">
                            <div class="image-box pt-5">
                                <img src="assets/images/landing/kerjasama-3.png" alt="kerjasama-3" />
                            </div>
                            <div class="profile-details text-center">
                                <h5>INTERNATIONAL CULTURAL COMMUNICATION CENTER MALAYSIA (ICCCM)</h5>
                                <div class="card-border mb-3" style="margin: 0 auto"></div>
                                <p>The parties have agreed to jointly establish the Chinese Language + Skill
                                    Training/Webinar/ Conference Centre.
                                    Both parties are allowed to recruit local & international students to register in
                                    the program.
                                </p>
                            </div>
                        </div>
                        <div class="card-kerjasama swiper-slide text-center">
                            <div class="image-box pt-5">
                                <img src="assets/images/landing/kerjasama-4.png" alt="kerjasama-4" />
                            </div>
                            <div class="profile-details text-center">
                                <h5>RSUD Mardi Waluyo Kota Blitar</h5>
                                <div class="card-border mb-3" style="margin: 0 auto"></div>
                                <p>Pelaksanaan Kegiatan BTCLS (Basic Trauma and Cradiac Life Support) Mahasiswa</p>
                            </div>
                        </div>
                        <div class="card-kerjasama swiper-slide text-center">
                            <div class="image-box pt-5">
                                <img src="assets/images/landing/kerjasama-3.png" alt="kerjasama-3" />
                            </div>
                            <div class="profile-details">
                                <h5>INTERNATIONAL CULTURAL COMMUNICATION CENTER MALAYSIA (ICCCM)</h5>
                                <div class="card-border mb-3" style="margin: 0 auto"></div>
                                <p>The parties have agreed to jointly establish the Chinese Language + Skill
                                    Training/Webinar/ Conference Centre.
                                    Both parties are allowed to recruit local & international students to register in
                                    the program.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-pagination"></div>
            </div>
        </div>
    </section>

    <!-- Footer Section -->
    <footer class="bg-footer" id="footer">
        <div class="row footer-title pt-5 pb-2 px-5 py-5">
            <div class="col-md-6">
                <h5 class="fw-bold">Menu Penting</h5>
            </div>
            <div class="col-md-6">
                <h5 class="fw-bold kontak-kami">Kontak Kami</h5>
            </div>
        </div>
        <div class="row footer-menu px-5 pb-2 pt-2 fw-bold">
            <div class="col-md-2">
                <ul>
                    <li class="mb-4"><a href="#profile">Profil</a></li>
                    <li class="mb-4"><a href="#program-studi">Program Studi</a></li>
                    <li class="mb-4"><a href="#tenaga-pengajar">Data Dosen</a></li>
                </ul>
            </div>
            <div class="col-md-2">
                <ul>
                    <li class="mb-4"><a href="#daftar-mou">Daftar MoU</a></li>
                    <li class="mb-4"><a href="#daftar-kerja-sama">Daftar Kerjasama</a></li>
                </ul>
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-5 mx-4">
                <ul>
                    <h5 class="fw-bold responsive-cont">Kontak Kami</h5>
                    <li class="mb-2"><img src="assets/images/Vector.png"> (0341) 351275</li>
                    <li class="mb-2"><img src="assets/images/Vector (1).png"> Jl. S. Supriadi No. 22, Sukun,
                        Kec. Sukun, Malang, Jawa Timur</li>
                    <li class="mb-2"><img src="assets/images/Vector (2).png"> informasi@itsk-soepraoen.ac.id</li>
                </ul>
            </div>
        </div>
        <div class="container-fluid py-3 bg-footer-end">
            <div class="row px-5">
                <div class="col-12 col-md-6 px-5 py-5 pt-0 pb-0">
                    <p class="mb-0 fw-bold">ITSK RS dr. Soepraoen | © All rights reserved 2024</p>
                </div>
                <div class="col-12 col-md-6 text-md-end px-5 py-5 pt-0 pb-0">
                    <ul class="list-inline mb-0">
                        <li class="list-inline-item"><a href="https://www.facebook.com/itsksoepraoenmalang/"><img src="assets/images/Vector (3).png"></a>
                        </li>
                        <li class="list-inline-item"><a href="https://www.instagram.com/itsk_soepraoen/?hl=en"><img src="assets/images/Vector (4).png"></a>
                        </li>
                        <li class="list-inline-item"><a href="https://x.com/itsk_soepraoen?mx=2"><img src="assets/images/Vector (5).png"></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>

    <script src="assets/js/swiper-bundle.min.js"></script>
    <script src="assets/js/landing.js"></script>
    <script src="assets/libs/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script>
        AOS.init();
    </script>
</body>

</html>