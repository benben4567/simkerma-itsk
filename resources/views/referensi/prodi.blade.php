@extends('base')

@section('title', 'SIMKERMA | Program Studi')

@section('konten')
<div class="page-title-box">
    <div class="row align-items-center">
        <div class="col-md-8">
            <h6 class="page-title">Data Program Studi</h6>
            {{-- Breadcrumb --}}
        </div>
        <div class="col-md-4">
            <div class="d-flex flex-column flex-md-row justify-content-end align-items-md-center gap-3">
                <a href="{{ route('prodi.export') }}" class="btn btn-primary waves-effect waves-light"><i class="mdi mdi-content-save"></i> Ekspor</a>
                <button type="button" class="btn btn-primary waves-effect waves-light" data-bs-toggle="modal" data-bs-target="#modalTambahProgramStudi"><i class="mdi mdi-plus"></i> Tambah Program Studi</button>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-12">
                <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Program Studi</th>
                            <th>Jumlah User</th>
                            <th>Edit</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($prodi as $index => $item)
                        <tr>
                            <td>{{ $index + 1 }}</td>
                            <td>{{ $item['nama'] }}</td>
                            <td>{{ $item['jumlahUser'] }}</td>
                            <td>
                                <div class="row gap">
                                    <div class="col">
                                        {{-- Edit Button --}}
                                        <button type="button" class="btn btn-warning waves-effect" data-bs-toggle="modal" data-bs-target="#modalEditProgramStudi" data-id="{{ $item['id'] }}" data-nama="{{ $item['nama'] }}"><i class="mdi mdi-pencil" style="color: black"></i></button>
                                        {{-- Delete Button --}}
                                        <form action="{{ route('prodi.destroy', $item['id']) }}" method="POST" class="d-inline delete-prodi">
                                            @csrf
                                            @method('DELETE')
                                            <button type="button" class="btn btn-danger waves-effect deleteBtn"><i class="mdi mdi-trash-can" style="color: black"></i></button>
                                        </form>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

{{-- MODAL TAMBAH PROGRAM STUDI --}}
<div id="modalTambahProgramStudi" class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form action="{{ route('prodi.store') }}" method="POST">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="myModalLabel2">Tambah Program Studi</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="col">
                        <div class="col-md-12 pb-3">
                            <label for="nama" class="form-label">Nama Program Studi</label>
                            <input type="text" class="form-control" id="nama" name="nama" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

{{-- MODAL EDIT PROGRAM STUDI --}}
<div id="modalEditProgramStudi" class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form id="editForm" method="POST" action="">
                @csrf
                @method('PUT')
                <!-- Tambahkan input tersembunyi untuk menandai permintaan PUT -->
                <input type="hidden" name="_method" value="PUT">
                <div class="modal-header">
                    <h5 class="modal-title" id="myModalLabel2">Edit Program Studi</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="col">
                        <div class="col-md-12 pb-3">
                            <label for="edit-nama" class="form-label">Nama Program Studi</label>
                            <input type="text" class="form-control" id="edit-nama" name="nama" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@push('script')
<script>
    // Delete confirmation
    $('.deleteBtn').on('click', function(e) {
        e.preventDefault();
        var form = $(this).closest('form');
        Swal.fire({
            title: 'Apakah Anda yakin?',
            text: "Data program studi ini akan dihapus!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, hapus!'
        }).then((result) => {
            if (result.isConfirmed) {
                form.submit();
            }
        })
    });

    // Edit modal fill form
    $('#modalEditProgramStudi').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget);
        var id = button.data('id');
        var nama = button.data('nama');

        var modal = $(this);
        modal.find('.modal-body #edit-nama').val(nama);

        // Make sure the action attribute is set correctly
        modal.find('#editForm').attr('action', '/program-studi/' + id);
    });
</script>
@endpush