<?php

namespace Database\Seeders;

use App\Models\JenisMitra;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class JenisMitraSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        collect(['Instansi Pemerintah, BUMN Dan/Atau BUMD', 'Institusi Pendidikan', 'Perusahaan', 'Organisasi', 'Rumah Sakit', 'UMKM'])->each(function ($jenis) {
            JenisMitra::create(['nama' => $jenis]);
        });
    }
}
