<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StatusKerjasama extends Model
{
    protected $table = 'status_kerjasama';

    public $timestamps = false;

    protected $fillable = [
        'nama',
    ];
}
