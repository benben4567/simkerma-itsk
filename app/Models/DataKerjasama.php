<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataKerjasama extends Model
{
    protected $table = 'data_kerjasama';

    protected $fillable = [
        'tanggal_mou',
        'tanggal_akhir',
        'deskripsi',
        'no_surat_instansi',
        'no_surat_mitra',
        'file',
        'jenis_dokumen',
        'status',
        'user_id',
        'mitra_id'
    ];

    public function mitra()
    {
        return $this->belongsTo(Mitra::class, 'mitra_id');
    }
}
