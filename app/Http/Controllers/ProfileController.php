<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\User;

class ProfileController extends Controller
{
    public function show(User $user)
    {
        $user = auth()->user();
        $fotoProfil = $user->foto ? asset('users/' . $user->foto) : asset('assets/images/users/img-default.png');
        return view('profil.profil', compact('user', 'fotoProfil'));
    }

    public function edit(User $user)
    {
        $user = auth()->user();
        $fotoProfil = $user->foto ? asset('users/' . $user->foto) : asset('assets/images/users/img-default.png');
        return view('profil.edit-profil', compact('user', 'fotoProfil'));
    }

    public function update(Request $request, User $user)
    {
        // Validasi data dari formulir
        $validatedData = $request->validate([
            'nama' => 'required|max:255',
            'email' => 'required|email|unique:users,email,' . $user->id,
            'prodi' => 'nullable|max:255',
        ]);

        $user->update($validatedData);

        return redirect()->route('profil.show')->with('success', 'Profil berhasil diperbarui.');
    }

    public function updateFoto(Request $request, User $user)
    {
        // Validasi data dari formulir
        $request->validate([
            'foto' => 'required|image|mimes:jpg,png,jpeg|max:2048',
        ]);

        // Hapus foto lama (jika ada)
        if ($user->foto && $request->hasFile('foto')) {
            Storage::disk('public')->delete($user->foto);
        }

        // Proses unggah foto baru dan simpan nama file di database
        if ($request->hasFile('foto')) {
            $fotoName = time() . '.' . $request->foto->getClientOriginalExtension();
            $request->foto->move(public_path('users'), $fotoName);
            $user->foto = $fotoName;
        } else {
            $defaultPhotoPath = 'users/default_foto.png';
            $user->foto = $defaultPhotoPath;
        }

        $user->save();

        return redirect()->route('profil.edit')->with('success', 'Foto profil berhasil diperbarui.');
    }
}
