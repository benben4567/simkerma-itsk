<?php

namespace App\Http\Controllers;

use App\Models\DataKerjasama;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke()
    {
        $hariIni = Carbon::now();
        $tujuhHariKedepan = $hariIni->copy()->addDays(7);
        
        if(Auth::user()->role === 'Admin' || Auth::user()->role === 'User'){
            $semuaKerjasama = DataKerjasama::join('users as u', 'data_kerjasama.user_id', '=', 'u.id')
                                            ->select('data_kerjasama.*', 'u.prodi')
                                            ->where('u.prodi', Auth::user()->prodi)
                                            ->count();
            $kerjasamaAktif = DataKerjasama::whereDate('tanggal_akhir', '>', now())
                            ->join('users as u', 'data_kerjasama.user_id', '=', 'u.id')
                            ->select('data_kerjasama.*', 'u.prodi')
                            ->where('u.prodi','=', Auth::user()->prodi)
                            ->count();
            $kerjasamaBerakhir = DataKerjasama::whereDate('tanggal_akhir', '<', now())
                            ->join('users as u', 'data_kerjasama.user_id', '=', 'u.id')
                            ->where('u.prodi','=' ,Auth::user()->prodi)
                            ->count();
            $kerjasamaAkanBerakhir = DataKerjasama::orderBy('created_at', 'desc')
                                ->join('mitra', 'data_kerjasama.mitra_id', '=', 'mitra.id')
                                ->join('users as u', 'data_kerjasama.user_id', '=', 'u.id')
                                ->select('data_kerjasama.*', 'mitra.nama_mitra', 'mitra.jenis_mitra', 'mitra.penanggung_jawab', 'mitra.email', 'mitra.no_telp', 'u.prodi as prodi_user')
                                ->where('tanggal_akhir','>', $hariIni)
                                ->where('tanggal_akhir','<',$tujuhHariKedepan)
                                ->where('u.prodi','=' ,Auth::user()->prodi)
                                ->count();
            $tanggalMouCounts = DataKerjasama::join('users as u', 'data_kerjasama.user_id', '=', 'u.id')
                                                ->select(DB::raw('YEAR(tanggal_mou) as year'), DB::raw('count(*) as jumlah_tanggal_mou'))
                                                ->where('u.prodi', '=', Auth::user()->prodi)
                                                ->groupBy(DB::raw('YEAR(tanggal_mou)'))
                                                ->get();

            $tanggalAkhirCounts = DataKerjasama::join('users as u', 'data_kerjasama.user_id', '=', 'u.id')
                                                ->select(DB::raw('YEAR(tanggal_akhir) as year'), DB::raw('count(*) as jumlah_tanggal_akhir'))
                                                ->where('u.prodi', '=', Auth::user()->prodi)
                                                ->groupBy(DB::raw('YEAR(tanggal_akhir)'))
                                                ->get();
        }
        else{
            $semuaKerjasama = DataKerjasama::count();
            $kerjasamaAktif = DataKerjasama::whereDate('tanggal_akhir', '>', now())->count();
            $kerjasamaBerakhir = DataKerjasama::whereDate('tanggal_akhir', '<', now())->count();
            $kerjasamaAkanBerakhir = DataKerjasama::orderBy('created_at', 'desc')
                                ->join('mitra', 'data_kerjasama.mitra_id', '=', 'mitra.id')
                                ->select('data_kerjasama.*', 'mitra.nama_mitra', 'mitra.jenis_mitra', 'mitra.penanggung_jawab', 'mitra.email', 'mitra.no_telp')
                                ->where('tanggal_akhir','>', $hariIni)
                                ->where('tanggal_akhir','<',$tujuhHariKedepan)
                                ->count();
            $tanggalMouCounts = DataKerjasama::select(DataKerjasama::raw('YEAR(tanggal_mou) as year'), DataKerjasama::raw('count(*) as jumlah_tanggal_mou'))
                                ->groupBy(DataKerjasama::raw('YEAR(tanggal_mou)'))
                                ->get();
            $tanggalAkhirCounts = DataKerjasama::select(DataKerjasama::raw('YEAR(tanggal_akhir) as year'), DataKerjasama::raw('count(*) as jumlah_tanggal_akhir'))
                                ->groupBy(DataKerjasama::raw('YEAR(tanggal_akhir)'))
                                ->get();
        }

        $mouCountsArray = $tanggalMouCounts->keyBy('year')->toArray();
        $akhirCountsArray = $tanggalAkhirCounts->keyBy('year')->toArray();


        $combinedCounts = [];

        foreach ($mouCountsArray as $year => $data) {
            $combinedCounts[] = [
                'year' => $data['year'],
                'jumlah_tanggal_mou' => $data['jumlah_tanggal_mou'],
                'jumlah_tanggal_akhir' => $akhirCountsArray[$year]['jumlah_tanggal_akhir'] ?? 0
            ];
        }
        
        // Menambahkan data jumlah_tanggal_akhir yang tidak ada di $mouCountsArray
        foreach ($akhirCountsArray as $year => $data) {
            if (!array_key_exists($year, $mouCountsArray)) {
                $combinedCounts[] = [
                    'year' => $data['year'],
                    'jumlah_tanggal_mou' => 0,
                    'jumlah_tanggal_akhir' => $data['jumlah_tanggal_akhir']
                ];
            }
        }

        usort($combinedCounts, function($a, $b) {
            return $a['year'] <=> $b['year'];
        });



        return view ('dashboard/admin-dashboard', compact('semuaKerjasama', 'kerjasamaAktif', 'kerjasamaBerakhir', 'kerjasamaAkanBerakhir', 'combinedCounts' ));
    }
}