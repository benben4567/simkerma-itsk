<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use RealRashid\SweetAlert\Facades\Alert;

class ChangePasswordController extends Controller
{
    public function show(User $user){
        $user = auth()->user();
        return view('auth.password.change', compact('user'));
    }
    public function store(Request $request){
            
    $request->validate([
        'old_password' => 'required',
        'new_password' => 'required|confirmed',
        'new_password_confirmation' => 'required'
    ]);
        
    $auth = Auth::user();

    // Check if the current password is valid
    if (!Hash::check($request->get('old_password'), $auth->password)) 
    {
        return back()->with('error', "Kata sandi lama tidak sesuai.");
    }

    // Check if the new password is the same as the current password
    if (strcmp($request->get('old_password'), $request->new_password) == 0) 
    {
        return redirect()->back()->with("error", "Kata sandi baru tidak boleh sama dengan kata sandi lama.");
    }
    
    $user =  User::find($auth->id);
    $user->password =  Hash::make($request->new_password);
    $user->save();
    return back()->with('success', "Kata sandi berhasil diubah.");
    }
}